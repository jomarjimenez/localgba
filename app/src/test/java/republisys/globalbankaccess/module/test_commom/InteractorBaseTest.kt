package republisys.globalbankaccess.module.test_commom

import com.google.gson.GsonBuilder
import mcig.base.BuildConfig
import mcig.module.data.typeAdapter.EntityTypeAdapterFactory
import republisys.globalbankaccess.module.domain.RestService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.junit.Before
import republisys.globalbankaccess.module.domain.HttpManager
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

// FIXME: This class should be in base module but the project can't compile when is placed there.

abstract class InteractorBaseTest : BaseTest() {

    lateinit var restService: RestService

    @Before
    open fun setUp() {
        restService = HttpManager.instance.restApi
    }
}
