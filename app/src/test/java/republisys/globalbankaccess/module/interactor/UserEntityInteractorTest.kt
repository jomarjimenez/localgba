package republisys.globalbankaccess.module.interactor

import io.reactivex.Completable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.subscribers.TestSubscriber

import org.junit.Before
import org.junit.Test

import org.mockito.Mock
import org.mockito.Mockito

import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify

import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.data.entity.local.CountryEntityTestUtils
import republisys.globalbankaccess.module.data.entity.remote.Result

import republisys.globalbankaccess.module.repository.SystemSettingsRepository
import republisys.globalbankaccess.module.test_commom.BaseTest
import java.util.ArrayList


/**
 * Created by republisys on 10/24/17.
 */
class UserEntityInteractorTest : BaseTest() {

    @Mock
    lateinit var repository: SystemSettingsRepository

    lateinit var interactor: SystemSettingsInteractor

    lateinit var arrangeBuilder: ArrangeBuilder

    lateinit var testSubscriber: TestSubscriber<List<CountryEntity>>

    @Before
    fun setUp() {
        interactor = SystemSettingsInteractor(repository)

        arrangeBuilder = ArrangeBuilder()
        testSubscriber = TestSubscriber()
    }

    @Test
    fun countryFromRepoAreUnwrappedAndPassedOn() {

        val testCountryList = createTestDraftList()
        interactor.getBehaviorStream(Option.none()).subscribe(testSubscriber)

        arrangeBuilder.emitCountryListFromRepo(Option.ofObj(testCountryList))

        testSubscriber.assertNotTerminated()
        testSubscriber.assertNoErrors()
    }

    @Test
    fun whenRepoIsEmptyFetchAndNoEmmissions() {

        arrangeBuilder.emitCountryListFromRepo(Option.none()).withSuccessFetch()

        interactor.getBehaviorStream(Option.none()).subscribe(testSubscriber)

        verify(repository)!!.fetchCountryDrafts()
        testSubscriber.assertNoValues()
        testSubscriber.assertNotTerminated()
    }

    @Test
    fun propagateFetchError(){
        val throwable = Mockito.mock(Throwable::class.java)

        arrangeBuilder.emitCountryListFromRepo(Option.none())
                .withFetchError(throwable)

        interactor.getBehaviorStream(Option.none()).subscribe(testSubscriber)

        verify(repository).fetchCountryDrafts()

        testSubscriber.assertNoValues()
        testSubscriber.assertError(throwable)

    }

    private fun createTestDraftList(): List<CountryEntity> {
        return object : ArrayList<CountryEntity>() {
            init {
                add(CountryEntityTestUtils.countryEntityTestBuilder().id("1").build())
                add(CountryEntityTestUtils.countryEntityTestBuilder().id("2").build())
                add(CountryEntityTestUtils.countryEntityTestBuilder().id("3").build())
            }
        }
    }

    inner class ArrangeBuilder {

        private val repoCountryStream = BehaviorProcessor.create<Option<List<CountryEntity>>>()

        init {
            `when`(repository.allCountryDrafts).thenReturn(repoCountryStream)
        }

        fun emitCountryListFromRepo(listOption: Option<List<CountryEntity>>): ArrangeBuilder {
            repoCountryStream.onNext(listOption)
            return this
        }

        fun withSuccessFetch(): ArrangeBuilder {
            `when`(repository.fetchCountryDrafts()).thenReturn(Completable.complete())
            return this
        }

        fun withFetchError(throwable: Throwable): ArrangeBuilder {
            `when`(repository.fetchCountryDrafts()).thenReturn(Completable.error(throwable))
            return this
        }

    }

}