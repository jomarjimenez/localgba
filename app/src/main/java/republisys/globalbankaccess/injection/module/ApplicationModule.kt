package republisys.globalbankaccess.injection.module

import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import republisys.globalbankaccess.BuildConfig

/**
 * Created by republisys on 10/23/17.
 */
@Module
class ApplicationModule {

    @Provides
    fun provideRealmConfiguration(): RealmConfiguration {

        var builder = RealmConfiguration.Builder()
        builder.inMemory()
        builder.name("default.realm")

        if (BuildConfig.DEBUG) {
            builder = builder.deleteRealmIfMigrationNeeded()
        }

        return builder.build()
    }

    @Provides
    fun provideRealm(realmConfiguration: RealmConfiguration): Realm {
        return Realm.getInstance(realmConfiguration)
    }

}