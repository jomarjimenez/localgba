package republisys.globalbankaccess.injection.module

import dagger.Module
import dagger.Provides
import mcig.base.common.providers.TimestampProvider
import mcig.base.data.cache.Cache
import mcig.base.data.store.MemoryReactiveStore
import mcig.base.data.store.ReactiveStore
import mcig.base.data.store.Store
import polanski.option.function.Func1
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import javax.inject.Singleton

import io.reactivex.functions.Function

/**
 * Created by republisys on 10/25/17.
 */
@Module
class DataModule {

    private val CACHE_MAX_AGE = (5 * 60 * 1000).toLong() // 5 minutes

    @Provides
    @Singleton
    internal fun provideCache(timestampProvider: TimestampProvider): Store.MemoryStore<String, CountryEntity> {
        return Cache(Function { it.id() }, timestampProvider, CACHE_MAX_AGE)
    }

    @Provides
    @Singleton
    internal fun provideReactiveStore(cache: Store.MemoryStore<String, CountryEntity>): ReactiveStore<String, CountryEntity> {
        return MemoryReactiveStore(Func1 { it.id() }, cache)
    }

}
