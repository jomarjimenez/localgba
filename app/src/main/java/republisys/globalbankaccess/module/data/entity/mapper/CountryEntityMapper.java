package republisys.globalbankaccess.module.data.entity.mapper;

import android.support.annotation.NonNull;

import io.reactivex.functions.Function;
import javax.inject.Inject;

import mcig.base.data.EssentialParamMissingException;
import republisys.globalbankaccess.module.data.entity.local.CountryEntity;
import republisys.globalbankaccess.module.data.entity.raw.CountryEntityRaw;

/**
 * Created by republisys on 10/25/17.
 */

public class CountryEntityMapper implements Function<CountryEntityRaw, CountryEntity> {

    @Inject
    public CountryEntityMapper() {
    }

    @Override
    public CountryEntity apply(CountryEntityRaw raw) {
        assertEssentialParams(raw);
        return CountryEntity.builder()
                .id(raw.id())
                .name(raw.name())
                .flag(raw.flag())
                .flag_rectangle(raw.flag_rectangle())
                .currency(raw.currency())
                .customer_bps(raw.customer_bps())
                .consumer_bps(raw.consumer_bps())
                .gross_margin(raw.gross_margin())
                .country_all(raw.country_all())
                .build();
    }

    private static void assertEssentialParams(@NonNull final CountryEntityRaw raw) {
        String missingParams = "";

        if (raw.id() == null || raw.id().isEmpty()){
            missingParams += ", id";
        }

        if (raw.name() == null || raw.name().isEmpty()){
            missingParams += ", name";
        }

        if (raw.flag() == null || raw.flag().isEmpty()) {
            missingParams += ", flag";
        }

        if (raw.flag_rectangle() == null || raw.flag_rectangle().isEmpty()) {
            missingParams += ", flag_rectangle";
        }

        if (raw.currency() == null || raw.currency().isEmpty()) {
            missingParams += ", currency";
        }

        if (raw.customer_bps() == null || raw.customer_bps().isEmpty()) {
            missingParams += ", customer_bps";
        }

        if (raw.consumer_bps() == null || raw.consumer_bps().isEmpty()) {
            missingParams += ", consumer_bps";
        }

        if (raw.gross_margin() == null || raw.gross_margin().isEmpty()) {
            missingParams += ", gross_margin";
        }

        if (raw.country_all() == null || raw.country_all().isEmpty()) {
            missingParams += ", country_all";
        }

        if (!missingParams.isEmpty()) {
            throw new EssentialParamMissingException(missingParams, raw);
        }
    }
}
