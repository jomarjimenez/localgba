package republisys.globalbankaccess.module.repository

import android.support.annotation.NonNull
import com.google.common.io.Files.map
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import mcig.base.data.store.ReactiveStore
import polanski.option.Option

import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.data.entity.mapper.CountryEntityMapper
import republisys.globalbankaccess.module.data.entity.raw.CountryEntityRaw
import republisys.globalbankaccess.module.domain.HttpManager
import republisys.globalbankaccess.module.domain.RestService
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by republisys on 9/25/17.
 */

/**
 *
 * */
@Singleton
open class UserRepository @Inject constructor(@NonNull val store: ReactiveStore<String, CountryEntity>, @NonNull val mapper: CountryEntityMapper, @NonNull val restApi: RestService) {

    fun getAll(): Flowable<Option<List<CountryEntity>>> {
        return store.all
    }

    fun fetchData(): Completable {
        return restApi.getCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMapObservable { Observable.fromIterable(it.dataList()) }
                .map(mapper)
                .toList()
                .doOnSuccess { store.replaceAll(it) }
                .toCompletable()
    }
}