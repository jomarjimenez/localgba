package republisys.globalbankaccess.module.presentation.login

import io.reactivex.Flowable
import io.reactivex.FlowableSubscriber
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mcig.base.presentation.base.BasePresenter
import org.reactivestreams.Subscription
import republisys.globalbankaccess.module.interactor.UserEntityInteractor
import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import republisys.globalbankaccess.module.interactor.SystemSettingsInteractor
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */
open class LoginPresenter @Inject constructor(private val systemSettingsInteractor: SystemSettingsInteractor) : BasePresenter<LoginView>() {

    override fun onPresenterCreated() {

    }

    fun fetchAll() {
        systemSettingsInteractor.getBehaviorStream(Option.none())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onSuccess(it) }, { onError(it.message!!) })
    }

    fun onSuccess(list: List<CountryEntity>) {
        view!!.showMainActivity()
    }

    fun onError(string: String) {
        view!!.onError(string)
    }

}