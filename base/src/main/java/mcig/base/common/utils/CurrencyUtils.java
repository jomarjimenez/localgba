package mcig.base.common.utils;

import java.text.NumberFormat;
import java.util.Locale;


public class CurrencyUtils {

    CurrencyUtils() { }

    public String formatAmount(final double amount) {
        return NumberFormat.getCurrencyInstance(Locale.GERMANY).format(amount);
    }

}
