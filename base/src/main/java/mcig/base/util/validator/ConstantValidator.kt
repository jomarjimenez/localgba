package mcig.base.util.validator

import mcig.base.util.validator.rules.MinLengthValidator
import mcig.base.util.validator.rules.RegexRule

/**
 * Created by republisys on 10/13/17.
 */

class ConstantValidator {

    companion object {

        fun PasswordValidator(): ValidatorBuilder.Builder<String> {
            return ValidatorBuilder.Builder<String>()
                    .withRule(RegexRule("[\\S]+", "Whitespace characters not allowed")) // THE ORDER IS IMPORTANT!
                    .withRule(RegexRule(".*[A-Z]+.*", "Must contain capital letters"))
                    .withRule(RegexRule(".*[0-9]+.*", "Must contain digits"))
                    .withRule(RegexRule(".*[a-z]+.*", "Must contain small letters"))
                    .withRule(MinLengthValidator(6, "Minimum length 6 characters"))
        }
    }

}

