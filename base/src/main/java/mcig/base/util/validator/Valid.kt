package mcig.base.util.validator

/**
 * Created by republisys on 10/12/17.
 */
interface Valid<T> {

    fun isValid(t: T)

}