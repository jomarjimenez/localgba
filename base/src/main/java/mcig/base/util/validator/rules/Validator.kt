package mcig.base.util.validator.rules

import mcig.base.util.validator.BaseValidator

/**
 * Created by republisys on 10/12/17.
 */
abstract class Validator<T> constructor(val error: String?) : BaseValidator<T> {

    override fun getErrorMessage(): String {
        return error!!
    }

}