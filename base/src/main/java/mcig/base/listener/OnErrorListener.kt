package estansaas.fonebayad.logic.mvp.listener

/**
 * Created by gerald.tayag on 2/9/2017.
 */

interface OnErrorListener<T> {
    fun onError(error: T)
}
